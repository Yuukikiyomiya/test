# Dockerfileハンズオン Dockerfile作成
```
mkdir ./dockerfile_test
cd dockerfile_test/
```
```
echo 'Dockerfile Contests' > index.html
```
```
vi Dockerfile
```
```
FROM nginx
COPY index.html /usr/share/nginx/html/
```
```
docker build -t test .
```
```
docker images
```
```
docker run -dit --name web -p 8080:80 test
```
```
docker export web > web.tar
```

# Dockerfileハンズオン リソース削除
```
docker stop web
```
```
docker rm web
```
```
docker image rm test
```
```
cd
rm -r dockerfile_test/
```