# kubesec使用①
```
curl -sSL https://github.com/shyiko/kubesec/releases/download/0.9.2/kubesec-0.9.2-linux-amd64 \
  -o kubesec && chmod a+x kubesec && sudo mv kubesec /usr/local/bin/ 
```
```
source <(kubesec completion bash)
```

# kubesec使用②
```
kubectl create secret generic sample-secret --from-literal=username=sample-user \
 --from-literal=password=sample-password \
 --dry-run=client -o yaml > kubesec-secret.yaml
```
```
cat kubesec-secret.yaml
```

# kubesec使用③
```
gpg --gen-key
```

# kubesec使用④
```
kubesec encrypt -i --key=pgp:<pubkeyコード> kubesec-secret.yaml
```
```
cat kubesec-secret.yaml
```

# kubesec使用⑤
```
kubesec decrypt -i kubesec-secret.yaml
```
```
cat kubesec-secret.yaml
```

# kubesec使用⑥
```
rm kubesec-secret.yaml
```
```
rm -rf .gnupg/
```