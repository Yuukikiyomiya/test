# PV、PVCハンズオン(1)StorageClass作成
```
cat > sc.yaml << EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
 name: standard
provisioner: kubernetes.io/no-privisioner
volumeBindingMode: WaitForFirstConsumer
EOF
```

# PV、PVCハンズオン(1) PersistentVolume作成
```
cat > pv.yaml << EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-bind
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 1Gi
  hostPath:
    path: /data/pv-bind
    type: DirectoryOrCreate
  persistentVolumeReclaimPolicy: Delete
  storageClassName: standard
EOF
```

# PV、PVCハンズオン(1) PersistentVolumeClaim作成
```
cat > pvc.yaml << EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-bind
spec:
  accessModes:
  - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
  storageClassName: standard
EOF
```

# PV、PVCハンズオン(1) 作成と確認①
```
kubectl apply -f sc.yaml -f pv.yaml -f pvc.yaml
```
```
kubectl get pv,pvc
```

# PV、PVCハンズオン(1) 作成と確認②
```
cat > pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: pv-bind-pod
spec:
  containers:
  - image: nginx
    name: nginx
    volumeMounts:
    - name: claim-volume
      mountPath: /usr/share/nginx/html/
  volumes:
  - name: claim-volume
    persistentVolumeClaim:
      claimName: pvc-bind
EOF
```

# PV、PVCハンズオン(1) 作成と確認③
```
kubectl apply -f pod.yaml
```
```
kubectl get pv,pvc
```

# PV、PVCハンズオン(1) 作成と確認④
```
kubectl delete -f pod.yaml -f pvc.yaml -f pv.yaml -f sc.yaml
```
