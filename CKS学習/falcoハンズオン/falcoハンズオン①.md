# falcoハンズオン：インストール
```
curl -s https://falco.org/repo/falcosecurity-packages.asc | apt-key add -
echo "deb https://download.falco.org/packages/deb stable main" | tee -a /etc/apt/sources.list.d/falcosecurity.list
apt-get update -y
apt-get install -y falco
```

# falcoハンズオン：falco起動
```
falco-driver-loader bpf
```
```
systemctl start falco-bpf.service
```

# falcoハンズオン：Podの作成と操作
```
kubectl run nginx --image=nginx
```
```
kubectl get pods -o wide
```
```
kubectl exec -it nginx -- sh
```

# falcoハンズオン：アラート出力確認
```
grep falco /var/log/syslog
```

# falcoハンズオン：アラート出力の編集①
```
cp /etc/falco/falco_rules.yaml /etc/falco/falco_rules.local.yaml
```
```
vi /etc/falco/falco_rules.local.yaml
```
```
systemctl restart falco-bpf.service
```

# falcoハンズオン：アラート出力の編集②
```
kubectl exec -it nginx -- sh
```
```
grep falco /var/log/syslog
```

# falcoハンズオン：falco停止
```
systemctl stop falco-bpf.service
```
