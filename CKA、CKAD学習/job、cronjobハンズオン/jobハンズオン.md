# jobハンズオン リソース作成
```
cat > job.yaml << EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: sleep-job
spec:
  completions: 5
  parallelism: 3
  template:
    spec:
      containers:
      - name: sleep
        image: busybox
        command: ["sleep"]
        args: ["10"]
      restartPolicy: Never
EOF
```

# jobハンズオン　リソース確認
```
kubectl get pods -w
```
```
kubectl apply -f job.yaml
```

# jobハンズオン　リソース確認と削除
```
kubectl get job
```
```
kubectl delete -f job.yaml
```