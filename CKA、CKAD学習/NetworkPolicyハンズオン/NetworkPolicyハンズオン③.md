# Networkpolicyハンズオン③リソース作成
```
kubectl label namespace default ns=default
```
```
cat > namespaceselector.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: namespaceselector-policy
  namespace: prod
spec:
  podSelector:
    matchLabels:
      app: nginx3
  policyTypes:
  - Ingress
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          ns: default
    ports:
    - protocol: TCP
      port: 80
EOF
```
```
cat > ipblock.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: ipblock-policy
  namespace: prod
spec:
  podSelector:
    matchLabels:
      app: nginx4
  policyTypes:
  - Ingress
  ingress:
  - from:
    - ipBlock:
        cidr: <PodIPアドレス>/32
    ports:
    - protocol: TCP
      port: 80
EOF
```
```
kubectl apply -f namespaceselector.yaml
kubectl apply -f ipblock.yaml
```
```
kubectl exec -it nginx1 -- curl --connect-timeout 3 <PodIPアドレス>
```

# Networkpolicyハンズオン③リソース削除
```
kubectl delete -f default-deny-all.yaml
kubectl delete -f prod-deny-all.yaml
kubectl delete -f podselector.yaml
kubectl delete -f namespaceselector.yaml
kubectl delete -f ipblock.yaml
```
```
kubectl delete -f nginx-pod.yaml
```
```
kubectl delete namespaces prod
```
```
kubectl label namespace default ns-
```
