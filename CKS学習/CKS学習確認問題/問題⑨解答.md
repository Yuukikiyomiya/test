```
vi Dockerfile

FROM nginx:1.15 ※ 変更
USER test ※ 変更
COPY ./html /usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf
RUN service nginx start
```
```
vi security-pod.yaml

apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo
spec:
  volumes:
  - name: sec-ctx-vol
    emptyDir: {}
  containers:
  - name: sec-ctx-demo
    image: gcr.io/google-samples/node-hello:1.0
    volumeMounts:
    - name: sec-ctx-vol
      mountPath: /data/demo
    securityContext:
      allowPrivilegeEscalation: false
      privileged: false ※ 変更
```