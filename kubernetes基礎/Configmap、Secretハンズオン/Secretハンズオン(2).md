# Secretハンズオン(2)作成と確認①
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
      - name: nginx
        image: nginx
EOF
```
```
cat > Nodeport.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: nodeport-service
spec:
  type: NodePort
  selector:
    app: nginx-deployment
  ports:
    - port: 80
      targetPort: 80
      nodePort: 30080
EOF
```
```
kubectl apply -f deployment.yaml
kubectl apply -f Nodeport.yaml
```

# Secretハンズオン(2)作成と確認②
```
<EC2のIPアドレス>:<NodePort番号>
```

# Secretハンズオン(2)作成と確認③
```
cat > basicmap.yaml << EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: basic-configmap
data:
  index.html: |
    Hello basic
  default.conf: |
    server {
     listen       80;
     listen  [::]:80;
     server_name  localhost;

     location / {
         root   /usr/share/nginx/html;
         index  index.html index.htm;
         auth_basic “Restricted”;
         auth_basic_user_file /etc/nginx/.htpasswd;
     }

     error_page   500 502 503 504  /50x.html;
     location = /50x.html {
         root   /usr/share/nginx/html;
     }
     }
EOF
```

# Secretハンズオン(2)作成と確認④
```
apt install apache2
```
```
htpasswd -nb test basic
```
```
echo -n 'ハッシュ値' | base64
```
```
cat > basic-secret.yaml << EOF
apiVersion: v1
kind: Secret
metadata:
  name: basic-secret
type: Opaque
data:
  .htpasswd: |
    エンコード値
EOF
```

# Secretハンズオン(2)作成と確認⑤
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
      - name: nginx
        image: nginx
        volumeMounts:
          - mountPath: /usr/share/nginx/html/index.html
            name: config
            subPath: index.html
          - mountPath: /etc/nginx/conf.d/default.conf
            name: config
            subPath: default.conf
          - mountPath: /etc/nginx/.htpasswd
            name: secret
            subPath: .htpasswd
      volumes:
        - name: config
          configMap:
            name: basic-configmap
            items:
              - key: index.html
                path: index.html
              - key: default.conf
                path: default.conf
        - name: secret
          secret:
            secretName: basic-secret
            items:
              - key: .htpasswd
                path: .htpasswd
EOF
```

# Secretハンズオン(2)作成と確認⑤
```
kubectl apply -f deployment.yaml -f Nodeport.yaml -f basicmap.yaml -f basic-secret.yaml
```
```
<EC2のIPアドレス>:<NodePort番号>
```
```
※curlでのbasic認証確認方法
curl -v --user test:basic 52.68.63.113:30080
```

# Secretハンズオン(2)作成と確認⑥
```
kubectl delete -f deployment.yaml -f Nodeport.yaml -f basicmap.yaml -f basic-secret.yaml
```
