# Deploymentハンズオン(4) 練習①
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-deployment
  labels:
    app: httpd
spec:
  replicas: 4
  selector:
    matchLabels:
      app: httpd
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
      - name: httpd
        image: httpd:2.4
EOF
```
```
kubectl apply -f deployment.yaml
```
```
kubectl get deployments test-deployment
kubectl describe deployments test-deployment
```

# Deploymentハンズオン(4) 練習②
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-deployment
  labels:
    app: httpd
spec:
  replicas: 5
  selector:
    matchLabels:
      app: httpd
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
      - name: httpd
        image: httpd:2.4.52
EOF
```
```
kubectl apply -f deployment.yaml
```
```
kubectl get deployments test-deployment
kubectl describe deployments test-deployment
```
```
kubectl delete -f deployment.yaml
```
