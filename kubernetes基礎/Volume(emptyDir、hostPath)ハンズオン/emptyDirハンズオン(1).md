# emptyDirハンズオン(1)作成と確認①
```
echo Hello nginx > index.html
```
```
cat > NodePort.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: NodePort
  selector:
    app: volume-pod
  ports:
    - port: 80
      targetPort: 80
      nodePort: 30080
EOF
```

# emptyDirハンズオン(1)作成と確認②
```
cat > pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: volume-pod
spec:
  containers:
  - image: nginx
    name: nginx
    volumeMounts:
    - mountPath: /usr/share/nginx/html/
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
EOF
```

# emptyDirハンズオン(1)作成と確認③
```
kubectl apply -f NodePort.yaml
kubectl apply -f pod.yaml
```
```
kubectl get pods -o wide
```

# emptyDirハンズオン(1)作成と確認④
```
kubectl describe service my-service
```

# emptyDirハンズオン(1)作成と確認⑤
```
kubectl cp index.html nginx:/usr/share/nginx/html/
```

# emptyDirハンズオン(1)作成と確認⑥
```
kubectl delete -f pod.yaml
kubectl apply -f pod.yaml
```
