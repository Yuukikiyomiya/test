# SecurityContextハンズオン リソース作成
```
kubectl run nginx --image nginx
```
```
kubectl exec nginx -- whoami
```
```
kubectl delete pods nginx
```
```
cat > securitycontext.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: securitycontext-pod
spec:
  securityContext:
    runAsUser: 1002
  containers:
  - image: ubuntu
    name: test1
    command: ["sleep", "5000"]
    securityContext:
      runAsUser: 1001

  - image: ubuntu
    name: test2
    command: ["sleep", "5000"]
EOF
```
```
kubectl apply -f securitycontext.yaml
```
```
kubectl exec securitycontext-pod -c test1 -- whoami
```
```
kubectl exec securitycontext-pod -c test2 -- whoami
```
```
kubectl delete -f securitycontext.yaml
```