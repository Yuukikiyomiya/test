# Podハンズオン(5)Multi-contaier作成②
```
cat > multipod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: multi-pod
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
EOF
```
```
kubectl apply -f multipod.yaml
```

# Podハンズオン(5)Multi-contaier作成③
```
kubectl get pods
```
```
kubectl describe pods
```

# Podハンズオン(5)Multi-contaier作成④
```
kubectl delete -f multipod.yaml
```
