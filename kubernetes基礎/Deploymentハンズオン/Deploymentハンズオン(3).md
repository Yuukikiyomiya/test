# Deploymentハンズオン(3) 作成と確認①
```
cat > rolling-update.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14
EOF
```

# Deploymentハンズオン(3) 作成と確認②
```
kubectl apply -f rolling-update.yaml
```
```
kubectl get replicasets
```
```
kubectl describe deployments nginx-deployment | grep -i image
```

# Deploymentハンズオン(3) 作成と確認③
```
vim rolling-update.yaml
```
```
kubectl get replicasets --watch
```

# Deploymentハンズオン(3) 作成と確認④
```
kubectl apply -f rolling-update.yaml
```
```
kubectl describe deployments nginx-deployment | grep -i image
```

# Deploymentハンズオン(3) 作成と確認⑤
```
kubectl rollout history deployment nginx-deployment
```

# Deploymentハンズオン(3) 作成と確認⑥
```
kubectl rollout history deployment nginx-deployment --revision 1
```

# Deploymentハンズオン(3) 作成と確認⑦
```
kubectl rollout undo deployment nginx-deployment
```
```
kubectl delete -f rolling-update.yaml
```