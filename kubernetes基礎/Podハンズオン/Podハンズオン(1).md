# Podハンズオン(1)Imperative (命令型)
```
kubectl run nginx --image=nginx
```

# Podハンズオン(1)Declarative (宣言型)
```
cat > redis.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: redis
spec:
  containers:
  - name: redis
    image: redis
EOF
```

```
kubectl create -f redis.yaml
```
```
kubectl get pods
```

# Podハンズオン(1)YAMLファイルの記述

kubernetes公式ドキュメントサイト
```
https://kubernetes.io/ja/docs/home/
```

Imperative (命令型)とDeclarative (宣言型)を組み合わせる。
```
kubectl run nginx2 --image=nginx --dry-run=client -o yaml > nginx2.yaml
```
```
vim nginx2.yaml
```
```
kubectl create -f nginx2.yaml
```

# Podハンズオン(1)リソース状態確認①
```
kubectl get pods nginx
```
```
kubectl get pods
```

# Podハンズオン(1)リソース状態確認②
```
kubectl get pods nginx2 -o wide
```
```
kubectl describe pods nginx2
```

# Podハンズオン(1)リソースの削除
```
kubectl delete pods nginx
```
```
kubectl delete -f redis.yaml
```
```
kubectl delete -f nginx2.yaml
```
```
kubectl get pods
```
