# DaemonSetハンズオン（1）作成と確認①
```
cat > daemonset.yaml << EOF
apiVersion: apps/v1
kind: DaemonSet
metadata: 
  name: sample-ds
spec:
  selector:
    matchLabels:
      name: sample-app
  template:
    metadata:
      labels:
        name: sample-app
    spec:
      containers:
      - name: nginx
        image: nginx
EOF
```
```
kubectl apply -f daemonset.yaml
```
```
kubectl get pods -o wide
```
```
kubectl delete -f daemonset.yaml
```
