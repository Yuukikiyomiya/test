# Secret情報の取得①
```
kubectl create secret generic sample-secret --from-literal=username=sample-user --from-literal=password=sample-password
```
```
kubectl get secrets sample-secret -o yaml
```

# Secret情報の取得②
```
echo "c2FtcGxlLXVzZXI=" | base64 -d
echo "c2FtcGxlLXBhc3N3b3Jk" | base64 -d
```