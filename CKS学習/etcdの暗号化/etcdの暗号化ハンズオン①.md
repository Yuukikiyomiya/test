# etcdデータ暗号化手順①
```
kubectl create secret generic etcd-secret --from-literal=username=sample-user3 \
 --from-literal=password=sample-password3
```
```
ETCDCTL_API=3 etcdctl \
   --cacert=/etc/kubernetes/pki/etcd/ca.crt   \
   --cert=/etc/kubernetes/pki/etcd/server.crt \
   --key=/etc/kubernetes/pki/etcd/server.key  \
   get /registry/secrets/default/etcd-secret | hexdump -C
```

# etcdデータ暗号化手順②
```
mkdir -p /etc/kubernetes/etcd
```
```
echo -n test-is-password | base64
```

# etcdデータ暗号化手順③
```
cat > /etc/kubernetes/etcd/ec.yaml << EOF
apiVersion: apiserver.config.k8s.io/v1
kind: EncryptionConfiguration
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: <BASE 64 ENCODED SECRET>
      - identity: {}
EOF
```

# etcdデータ暗号化手順④
```
cp /etc/kubernetes/manifests/kube-apiserver.yaml /etc/kubernetes/
```
```
vi /etc/kubernetes/manifests/kube-apiserver.yaml

    - --encryption-provider-config=/etc/kubernetes/etcd/ec.yaml

    - mountPath: /etc/kubernetes/etcd
      name: etcd
      readOnly: true

  - hostPath:
      path: /etc/kubernetes/etcd
      type: DirectoryOrCreate
    name: etcd
```

# etcdデータ暗号化手順⑤
```
kubectl get secrets etcd-secret -o yaml | kubectl replace -f -
```
```
ETCDCTL_API=3 etcdctl \
   --cacert=/etc/kubernetes/pki/etcd/ca.crt   \
   --cert=/etc/kubernetes/pki/etcd/server.crt \
   --key=/etc/kubernetes/pki/etcd/server.key  \
   get /registry/secrets/default/etcd-secret | hexdump -C
```
