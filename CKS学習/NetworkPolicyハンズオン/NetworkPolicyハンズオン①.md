# Networkpolicyハンズオン①リソース作成
```
kubectl create namespace prod
```
```
cat > nginx-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx1
  labels:
    app: nginx1
spec:
  containers:
    - name: nginx1
      image: nginx
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx2
  labels:
    app: nginx2
spec:
  containers:
    - name: nginx2
      image: nginx
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx3
  namespace: prod
  labels:
    app: nginx3
spec:
  containers:
    - name: nginx3
      image: nginx
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx4
  namespace: prod
  labels:
    app: nginx4
spec:
  containers:
    - name: nginx4
      image: nginx
---
EOF
```
```
kubectl apply -f nginx-pod.yaml
```
```
kubectl get pods -o wide -n default
kubectl get pods -o wide -n prod
```
```
kubectl exec -it nginx1 -- curl <PodIPアドレス>
```
```
cat > default-deny-all.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-deny-all
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  - Egress
  egress:
  - {}
EOF
```
```
cat > prod-deny-all.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: prod-deny-all
  namespace: prod
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  - Egress
  egress:
  - {}
EOF
```
```
kubectl apply -f default-deny-all.yaml
kubectl apply -f prod-deny-all.yaml
```
```
kubectl exec -it nginx1 -- curl --connect-timeout 3 <PodIPアドレス>
```
