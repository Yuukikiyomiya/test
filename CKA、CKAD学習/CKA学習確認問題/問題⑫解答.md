```
cat > testpod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-server
spec:
  containers:
  - image: nginx
    name: nginx
    volumeMounts:
    - name: rwo-volume
      mountPath: /usr/share/nginx/html/
  volumes:
  - name: rwo-volume
    persistentVolumeClaim:
      claimName: rwo-pvc
EOF
```
```
kubectl apply -f testpod.yaml -f testpvc.yaml -f testpv.yaml -f sc.yaml
```
```
kubectl get pv,pvc
```
```
kubectl edit pvc rwo-pvc --save-config
```
