# PSA適用
```
cat > psa-ns.yaml << EOF
apiVersion: v1
kind: Namespace
metadata:
  name: psa-ns
  labels:
    pod-security.kubernetes.io/enforce: privileged
EOF
```
```
kubectl apply -f psa-ns.yaml
```

# Pod作成
```
cat > psa-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: psa-pod
  namespace: psa-ns
spec:
  containers:
  - name: nginx
    image: nginx
    securityContext:
        privileged: true
EOF
```
```
kubectl apply -f psa-pod.yaml
```

# PSA変更
```
kubectl delete -f psa-pod.yaml
```
```
cat > psa-ns.yaml << EOF
apiVersion: v1
kind: Namespace
metadata:
  name: psa-ns
  labels:
    pod-security.kubernetes.io/enforce: baseline
EOF
```
```
kubectl apply -f psa-ns.yaml
```

# PSA検証
```
kubectl apply -f psa-pod.yaml
```

# リソース削除
```
kubectl delete -f psa-ns.yaml
```