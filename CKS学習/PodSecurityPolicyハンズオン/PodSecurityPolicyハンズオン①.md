# PSP有効化
```
vi /etc/kubernetes/manifests/kube-apiserver.yaml
```

# PSPリソース作成
```
cat > test-psp.yaml << EOF
apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: test-psp
spec:
  privileged: false
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  runAsUser:
    rule: RunAsAny
  fsGroup:
    rule: RunAsAny
  volumes:
  - '*'
EOF
```
```
kubectl apply -f test-psp.yaml
```
```
kubectl get psp
```

# PSP適用role作成
```
kubectl create role psp-role --verb=use --resource=podsecuritypolicy --resource-name=test-psp
```

# SA作成
```
kubectl create serviceaccount psp-user
```
```
kubectl create rolebinding psp-binding --clusterrole=edit --serviceaccount=default:psp-user
```

# PSPの適用
```
kubectl create rolebinding psp-role-binding --role=psp-role --serviceaccount=default:psp-user
```

# ポリシー検証
```
cat > psp-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: psp-pod
spec:
  containers:
    - name: nginx
      image: nginx
EOF
```
```
kubectl --as=system:serviceaccount:default:psp-user apply -f psp-pod.yaml
```
```
kubectl delete -f psp-pod.yaml
```
```
cat > psp-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: psp-pod
spec:
  containers:
    - name: nginx
      image: nginx
      securityContext:
        privileged: true
EOF
```
```
kubectl --as=system:serviceaccount:default:psp-user apply -f psp-pod.yaml
```
```
kubectl delete -f psp-pod.yaml
kubectl delete rolebindings psp-binding
kubectl delete role psp-role
kubectl delete -f test-psp.yaml
kubectl delete serviceaccounts psp-user

```