# NodeSelectorハンズオン②リソース作成
```
cat > nodeselector.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-selector
spec:
  containers:
  - name: nginx
    image: nginx
  nodeSelector:
    worker: node
EOF
```
```
kubectl apply -f nodeselector.yaml
```
```
kubectl get pods
```
```
kubectl get node
```
```
kubectl label node/<node名> worker=node
```
```
kubectl get pods -o wide
```

# NodeSelectorハンズオン③リソース削除
```
kubectl label node/<node名> worker-
```
```
kubectl delete -f nodeselector.yaml
```
