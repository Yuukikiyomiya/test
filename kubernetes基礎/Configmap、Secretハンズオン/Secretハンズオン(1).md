# Secretハンズオン(1)作成方法②
```
kubectl create secret generic test-secret --from-literal=username=test --from-literal=passwd=secret
```
```
kubectl describe secrets test-secret
```

# Secretハンズオン(1)作成方法③
```
kubectl get secret test-secret -o yaml
```
```
kubectl delete secrets test-secret
```

# Secretハンズオン(1)作成方法④
```
echo -n test | base64
echo -n secret | base64
```
```
cat > secret.yaml << EOF
apiVersion: v1
kind: Secret
metadata:
  name: test-secret
type: Opaque
data:
  username: dGVzdA==
  passwd: c2VjcmVO
EOF
```
```
kubectl apply -f secret.yaml
```

# Secretハンズオン(1)作成方法⑤
```
kubectl describe secret test-secret
```
```
kubectl get secret test-secret -o yaml
```
```
kubectl delete -f secret.yaml
```
