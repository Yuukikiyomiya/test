# hostPathハンズオン(1)作成と確認①
```
kubectl delete -f pod.yaml
```
```
cat > pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: volume-pod
spec:
  containers:
  - image: nginx
    name: nginx
    volumeMounts:
    - mountPath: /usr/share/nginx/html/
      name: cache-volume
  volumes:
  - name: cache-volume
    hostPath:
      path: /etc
      type: Directory
EOF
```

# hostPathハンズオン(1)作成と確認②
```
kubectl apply -f pod.yaml
```
```
kubectl cp index.html nginx:/usr/share/nginx/html/
```

# hostPathハンズオン(1)作成と確認③
```
kubectl delete -f pod.yaml
kubectl apply -f pod.yaml
```

# hostPathハンズオン(1)作成と確認④
```
kubectl exec -it nginx -- rm /usr/share/nginx/html/index.html
```
```
kubectl delete -f pod.yaml
kubectl delete -f NodePort.yaml
```
