# NodeAffinityハンズオン②リソース作成
```
cat > nodeaffinity.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-affinity
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: nodeaffinity
            operator: In
            values:
            - required
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: worker
            operator: In
            values:
            - node1
  containers:
  - name: nginx
    image: nginx
EOF
```
```
kubectl apply -f nodeaffinity.yaml
```
```
kubectl get pods
```
```
kubectl get node
```
```
kubectl label node/<node名1台目> nodeaffinity=required worker=node1
```
```
kubectl label node/<node名2台目> nodeaffinity=required worker=node2
```
```
kubectl get pods -o wide
```
```
cat > nodeaffinity.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-affinity
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: nodeaffinity
            operator: In
            values:
            - required
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: worker
            operator: In
            values:
            - node2
  containers:
  - name: nginx
    image: nginx
EOF
```
```
kubectl delete -f nodeaffinity.yaml
kubectl apply -f nodeaffinity.yaml
```
```
kubectl get pods -o wide
```

# NodeAffinityハンズオン③リソース削除
```
kubectl label node/<node名1台目> nodeaffinity- worker-
```
```
kubectl label node/<node名2台目> nodeaffinity- worker-
```
```
kubectl delete -f nodeaffinity.yaml
```
