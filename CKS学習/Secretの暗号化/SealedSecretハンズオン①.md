# SealedSecretの使用①
```
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.18.0/kubeseal-0.18.0-linux-amd64.tar.gz
tar -xvzf kubeseal-0.18.0-linux-amd64.tar.gz kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```
```
kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.18.0/controller.yaml
```

# SealedSecretの使用②
```
kubectl create secret generic sample-secret --from-literal=username=sample-user2 \
 --from-literal=password=sample-password2 \
 --dry-run=client -o yaml > kubeseal-secret.yaml
```
```
cat kubeseal-secret.yaml
```

# SealedSecretの使用③
```
kubeseal -o yaml < kubeseal-secret.yaml > sealed-secret.yaml
```
```
cat sealed-secret.yaml
```

# SealedSecretの使用④
```
kubectl apply -f sealed-secret.yaml
```
```
kubectl get secrets
```

# SealedSecretの使用⑤
```
kubectl delete -f sealed-secret.yaml
```
