# Podハンズオン(2)ラベルの付与①
```
kubectl run nginx --image=nginx --labels=app=service
```
```
kubectl get pod --show-labels
```

# Podハンズオン(2)ラベルの付与②
```
cat > test.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: service
  name: test-nginx
spec:
  containers:
  - image: nginx
    name: nginx
EOF
```
```
kubectl create -f test.yaml
```
```
kubectl delete -f test.yaml
```
```
kubectl delete pods nginx
```
