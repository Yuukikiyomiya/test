```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: test-deployment
  name: test-deployment
  namespace: test
spec:
  replicas: 2
  selector:
    matchLabels:
      app: test
  template:
    metadata:
      labels:
        app: test
    spec:
      containers:
      - image: nginx:1.14
        name: nginx
EOF
```
```
kubectl apply -f deployment.yaml
```
```
kubectl get deployment test-deployment -n test
```
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: test-deployment
  name: test-deployment
  namespace: test
spec:
  replicas: 4
  selector:
    matchLabels:
      app: test
  template:
    metadata:
      labels:
        app: test
    spec:
      containers:
      - image: nginx:1.15
        name: nginx
EOF
```
```
kubectl apply -f deployment.yaml
```
```
kubectl get deployment test-deployment -n test
```
```
kubectl describe deployment test-deployment -n test
```
```
kubectl rollout undo deployment test-deployment -n test
```
