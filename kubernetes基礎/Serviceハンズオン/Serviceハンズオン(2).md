# Serviceハンズオン(2)NodePort①
```
kubectl create deployment nginx-deployment --image=nginx --replicas=2
```
```
kubectl expose deployment nginx-deployment --type=NodePort --port=80 --target-port=80 --name=nodeport-service
```

# Serviceハンズオン(2)NodePort②
```
cat > Nodeport.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: nodeport-service
spec:
  type: NodePort
  selector:
    app: nginx-deployment
  ports:
    - port: 80
      targetPort: 80
      nodePort: 30080
EOF
```
```
kubectl apply -f Nodeport.yaml
```

# Serviceハンズオン(2)NodePort③
```
kubectl get service nodeport-service
```
```
kubectl describe service nodeport-service
```

# Serviceハンズオン(2)NodePort④
```
<EC2のIPアドレス>:<NodePort番号>
```

# Serviceハンズオン(2)NodePort⑤
```
kubectl delete service nodeport-service
kubectl delete deployments nginx-deployment
```
