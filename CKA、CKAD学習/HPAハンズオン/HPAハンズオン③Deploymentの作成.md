# HPAハンズオン③Deploymentの作成
```
cat > hpa-deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hpa-example
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hpa-example
  template:
    metadata:
      labels:
        app: hpa-example
    spec:
      containers:
      - name: hpa-container
        image: polinux/stress
        resources:
          requests:
            cpu: 200m
          limits:
            cpu: 500m
        command: ["stress"]
        args: ["--cpu", "2", "--timeout", "300"]
EOF
```