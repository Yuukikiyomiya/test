# Initコンテナハンズオン②リソース作成
```
cat > init-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: init-pod
  labels:
    init: pod
spec:
  containers:
  - name: nginx
    image: nginx
    volumeMounts:
    - name: workdir
      mountPath: /usr/share/nginx/html
  initContainers:
  - name: busybox
    image: busybox
    command: ["/bin/sh"]
    args: ["-c", "echo 'init test page' >> /work-dir/index.html"]
    volumeMounts:
    - name: workdir
      mountPath: "/work-dir"
  volumes:
  - name: workdir
    emptyDir: {}
EOF
```
```
cat > init-service.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: init-service
spec:
  ports:
  - port: 80
    targetPort: 80
    nodePort: 30080
  selector:
    init: pod
  type: NodePort
EOF
```
```
kubectl apply -f init-pod.yaml
kubectl apply -f init-service.yaml
```
```
kubectl describe pods init-pod
```

# Initコンテナハンズオン③リソース削除
```
kubectl delete -f init-pod.yaml
kubectl delete -f init-service.yaml
```
