# Serviceハンズオン(1)ClusterIP①
```
kubectl create deployment nginx-deployment --image=nginx --replicas=2
```
```
kubectl expose deployment nginx-deployment --type=ClusterIP --port=80 --target-port=80 --name=clusterip-service
```

# Serviceハンズオン(1)ClusterIP②
```
cat > ClusterIP.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: clusterip-service
spec:
  type: ClusterIP
  selector:
    app: nginx-deployment
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
EOF
```
```
kubectl get service clusterip-service
```

# Serviceハンズオン(1)ClusterIP③
```
kubectl describe service clusterip-service
```

# Serviceハンズオン(1)ClusterIP④
```
kubectl run -it busy --rm --image=busybox sh
```

# Serviceハンズオン(1)ClusterIP⑤
```
wget -q -O - http://clusterip-service
```
```
exit
```

# Serviceハンズオン(1)ClusterIP⑥
```
kubectl delete service clusterip-service
```
```
kubectl delete deployments nginx-deployment
```
