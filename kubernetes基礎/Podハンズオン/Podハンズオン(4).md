# Podハンズオン(4)エラー検証①
```
kubectl run test --image=nginx777 --dry-run=client -o yaml > test.yaml
```
```
vim test.yaml
```

# Podハンズオン(4)エラー検証②
```
kubectl create -f test.yaml
```
```
kubectl get pods
```

# Podハンズオン(4)エラー検証③
```
kubectl describe pods test
```

# Podハンズオン(4)エラー検証④
```
vim test.yaml
```
```
kubectl apply -f test.yaml
```
```
kubectl get pods
```
```
kubectl delete -f test.yaml
```

# Podハンズオン(4)エラー検証⑤
```
cat > test.yaml << EOF
apiVersion: v1
kind: pod
metadata:
  labels:
    run: test
  name: test
spec:
  containers:
  - image: nginx
    name: test
EOF
```
```
kubectl apply -f test.yaml
```

# Podハンズオン(4)エラー検証⑥
```
cat > test.yaml << EOF
apiVersion: v1
kind: Pod
 metadata:
  labels:
    run: test
  name: test
spec:
  containers:
  - image: nginx
    name: test
EOF
```
```
kubectl apply -f test.yaml
```
