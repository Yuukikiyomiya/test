```
kubectl create namespace test
```
```
kubectl create serviceaccount test-account --namespace=test
```
```
kubectl create clusterrole test-role --verb=create --resource=deployments,statefulsets,daemonsets
```
```
kubectl create rolebinding test-role-binding --clusterrole=test-role --serviceaccount=test:test-account --namespace=test
```
```
kubectl auth can-i create deployment --as=system:serviceaccount:test:test-account --namespace=test
```
