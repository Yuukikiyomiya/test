# Trivyハンズオン：インストール
```
apt-get install wget apt-transport-https gnupg lsb-release -y
```
```
wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | sudo tee /usr/share/keyrings/trivy.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | sudo tee -a /etc/apt/sources.list.d/trivy.list
apt-get update
apt-get install trivy
```

# Trivyハンズオン：Trivyコマンド実行
```
trivy image nginx
```
```
trivy image --severity HIGH,CRITICAL nginx
```

# Trivyハンズオン：Trivyの削除
```
apt-get remove trivy -y
```