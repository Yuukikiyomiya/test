# Configmapハンズオン(2)作成と確認①
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
      - name: nginx
        image: nginx
EOF
```
```
cat > Nodeport.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: nodeport-service
spec:
  type: NodePort
  selector:
    app: nginx-deployment
  ports:
    - port: 80
      targetPort: 80
      nodePort: 30080
EOF
```
```
kubectl apply -f deployment.yaml
kubectl apply -f Nodeport.yaml
```

# Configmapハンズオン(2)作成と確認②
```
<EC2のIPアドレス>:<NodePort番号>
```

# Configmapハンズオン(2)作成と確認③
```
cat > configmap.yaml << EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-configmap
data:
  index.html: |
    Hello world
EOF
```
```
kubectl apply -f configmap.yaml
```

# Configmapハンズオン(2)作成と確認④
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
      - name: nginx
        image: nginx
        volumeMounts:
          - mountPath: /usr/share/nginx/html/
            name: config
      volumes:
        - name: config
          configMap:
            name: nginx-configmap
EOF
```

# Configmapハンズオン(2)作成と確認⑤
```
kubectl apply -f deployment.yaml
```
```
<EC2のIPアドレス>:<NodePort番号>
```

# Configmapハンズオン(2)作成と確認⑥
```
kubectl delete -f deployment.yaml
kubectl delete -f Nodeport.yaml
kubectl delete -f configmap.yaml
```
