# namespaceハンズオン(1)リソース確認
```
kubectl get namespaces
```
```
kubectl run nginx --image=nginx
```
```
kubectl get pods
```

# namespaceハンズオン(1)リソース作成
```
kubectl create namespace prod
```
```
kubectl run nginx2 --image=nginx -n prod
```
```
kubectl get pods -n prod
```
```
cat > ClusterIP.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: clusterip-service
  namespace: prod
spec:
  type: ClusterIP
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
EOF
```
```
kubectl apply -f ClusterIP.yaml
```
```
kubectl get svc -n prod
```
```
kubectl run -it busy --rm --image=busybox sh -n prod
```
```
cat /etc/resolv.conf
```
```
nslookup <service ip>
```
```
exit
```

# namespaceハンズオン(1)リソース削除
```
kubectl delete pods nginx
kubectl delete pods nginx2 -n prod
kubectl delete -f ClusterIP.yaml
kubectl delete namespaces prod
```
