# NFSサーバーのインストール　※Master nodeで実行
```
apt install nfs-kernel-server
```
# ディレクトリ作成　 ※Master nodeで実行
```
mkdir /nfs
```
# マウント許可設定　 ※Master nodeで実行
```
cat << EOF > /etc/exports
/nfs <masterIP>(rw,no_root_squash) <node1IP>(rw,no_root_squash) <node2IP>(rw,no_root_squash)
EOF
```
# NFSサーバー起動　 ※Master nodeで実行
```
systemctl start nfs-server.service
systemctl enable nfs-server.service
```
# nfsクライアントインストール　※各node
```
apt-get install nfs-common
```
# Helmインストール
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
# nfs-subdir-external-provisioner のインストール
```
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
```
```
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --set nfs.server=<nfsのIP> \
    --set nfs.path=/nfs \
    --set storageClass.name=dynamic-nfs-storage
```
# 作成されたリソース確認
```
kubectl get storageclass
```
```
kubectl get pod ※プロビジョナーPodがrunningにならない場合は、nfs-serverをリスタート
```
# Headless ServiceのYAMLファイル作成
```
cat > headless.yaml << EOF
apiVersion: v1
kind: Service
metadata:
  name: headless-svc
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: test-sts
  clusterIP: None
  selector:
    app: nginx
EOF
```
# StatefulSetのYAMLファイル作成
```
cat > statefulset.yaml << EOF
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: test-sts
spec:
  serviceName: headless-svc
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: registry.k8s.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: test-sts
        volumeMounts:
        - name: pvc
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: pvc
    spec:
      storageClassName: dynamic-nfs-storage
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
EOF
```
# リソースのデプロイ
```
kubectl apply -f headless.yaml -f statefulset.yaml
```
# リソース確認
```
kubectl get statefulset test-sts
kubectl get pods -l app=nginx
kubectl get pvc -l app=nginx
```
# 名前解決検証用Pod作成
```
kubectl run -it --image busybox:1.28 dns-test --restart=Never --rm
```
# 検証用Podのターミナルで名前解決検証
```
nslookup test-sts-0.headless-svc
```
```
exit
```
# テキストデータ作成
```
for i in 0 1; do kubectl exec "test-sts-$i" -- sh -c 'echo "$(hostname)" > /usr/share/nginx/html/index.html'; done
```
# テキストデータの確認
```
for i in 0 1; do kubectl exec -i -t "test-sts-$i" -- curl http://localhost/; done
```
# StatefulSetの削除
```
kubectl delete -f statefulset.yaml
```
# StatefulSetの再作成
```
kubectl apply -f statefulset.yaml
```
# テキストデータの再確認
```
for i in 0 1; do kubectl exec -i -t "test-sts-$i" -- curl http://localhost/; done
```
# StatefulSetとHeadless service削除
```
kubectl delete -f statefulset.yaml -f headless.yaml
```
# PVCの削除
```
kubectl delete pvc -l app=nginx
```
# nfs-subdir-external-provisioner のアンインストール
```
helm uninstall nfs-subdir-external-provisioner
```