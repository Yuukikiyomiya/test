# Networkpolicyハンズオン①リソース作成
```
cat > podselector.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: podselector-policy
spec:
  podSelector:
    matchLabels:
      app: nginx2
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          app: nginx1
    ports:
    - protocol: TCP
      port: 80
EOF
```
```
kubectl apply -f podselector.yaml
```
```
kubectl exec -it nginx1 -- curl --connect-timeout 3 <PodIPアドレス>
```
