# Deploymentハンズオン(2) 作成と確認①
```
cat > deployment.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
EOF
```

# Deploymentハンズオン(2) 作成と確認②
```
kubectl create deployment nginx-deployment --image=nginx --replicas=3 --dry-run=client -o yaml > deployment.yaml
```
```
kubectl apply -f deployment.yaml
```

# Deploymentハンズオン(2) 作成と確認③
```
kubectl get deployments
```
```
vim deployment.yaml
```

# Deploymentハンズオン(2) 作成と確認④
```
kubectl apply -f deployment.yaml
```
```
kubectl get deployments
```
```
kubectl scale --replicas=2 -f deployment.yaml
```
```
kubectl get deployments
```

# Deploymentハンズオン(2) 作成と確認⑤
```
kubectl delete -f deployment.yaml
```
```
kubectl get deployments
```
