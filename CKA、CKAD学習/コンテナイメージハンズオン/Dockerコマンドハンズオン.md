# Dockerコマンドハンズオン 準備
```
docker pull nginx:latest
```

# Dockerコマンドハンズオン コンテナ作成
```
docker pull nginx:latest
```
```
docker images
```
```
docker create -it nginx /bin/bash
```
```
docker ps -a
```
```
docker start <コンテナID>
```
```
docker ps
```
```
docker run --name test -dit nginx:latest /bin/bash
```

# Dockerコマンドハンズオン コンテナ削除
```
docker ps
```
```
docker rm test <コンテナ名>
```
```
docker image rm nginx:latest
```