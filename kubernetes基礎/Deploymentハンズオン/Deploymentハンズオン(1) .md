# Deploymentハンズオン(1) 作成と確認①
```
kubectl create deployment nginx-deploy --image=nginx --replicas=2
```
```
kubectl get deployment
```

# Deploymentハンズオン(1) 作成と確認②
```
kubectl get replicaset
```
```
kubectl get pods
```

# Deploymentハンズオン(1) 作成と確認③
```
kubectl describe pod | grep -i label
```

# Deploymentハンズオン(1) 作成と確認④
```
kubectl describe deployments nginx-deploy
```
```
kubectl get pods
```
```
kubectl delete pods <Pod名>
```

# Deploymentハンズオン(1) 作成と確認⑤
```
kubectl get pods
```
```
kubectl delete deployments nginx-deploy
```
```
kubectl get deployments
```
