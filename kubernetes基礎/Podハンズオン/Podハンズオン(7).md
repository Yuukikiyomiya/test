# Podハンズオン(7)練習①
```
cat > test.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: label
  name: nginx
spec:
  containers:
  - image: nginx
    name: nginx
    env:
    - name: test_env
      value: "sample"
EOF
```
```
kubectl apply -f test.yaml
```
```
kubectl get pods
```
```
kubectl get pods -o wide
```
```
kubectl describe pods
```

# Podハンズオン(7)練習②
```
cat > multipod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: multi-pod
spec:
  containers:
  - name: redis
    image: redis
  - name: httpd
    image: httpd
EOF
```
```
kubectl apply -f multipod.yaml
```
```
kubectl get pods
```
```
kubectl get pods -o wide
```
```
kubectl describe pods
```
```
kubectl delete -f test.yaml
kubectl delete -f multipod.yaml
```
