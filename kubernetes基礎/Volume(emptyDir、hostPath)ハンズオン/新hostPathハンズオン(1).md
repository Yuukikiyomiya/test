# hostPathハンズオン(1)作成と確認①
```
cat > volume-daemonset.yaml << EOF
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: volume-daemonset
spec:
  selector:
    matchLabels:
      app: volume-daemonset
  template:
    metadata:
      labels:
        app: volume-daemonset
    spec:
      containers:
      - name: date
        image: alpine
        command: ["sh", "-c"]
        args:
        - |
          exec >> /var/log/date/output.log
          echo -n 'Start at: '
          while true; do
            date
            sleep 10
          done
        volumeMounts:
        - name: log-volume
          mountPath: /var/log/date
      volumes:
      - name: log-volume
        hostPath:
          path: /tmp
EOF
```

# hostPathハンズオン(1)作成と確認②
```
kubectl apply -f volume-daemonset.yaml
```
```
kubectl get pods -o wide
```
```
kubectl exec -it <Pod名> -- head /var/log/date/output.log
```

# hostPathハンズオン(1)作成と確認③
```
kubectl delete -f volume-daemonset.yaml
kubectl apply -f volume-daemonset.yaml
```
```
kubectl get pods -o wide
```
```
kubectl exec -it <Pod名> -- head /var/log/date/output.log
```
```
kubectl exec -it <Pod名> -- rm /var/log/date/output.log
```
```
kubectl delete -f volume-daemonset.yaml
```
