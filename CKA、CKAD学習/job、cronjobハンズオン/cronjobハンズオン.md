# CronJobハンズオン リソース作成
```
cat > cronjob.yaml << EOF
apiVersion: batch/v1
kind: CronJob
metadata:
  name: test-cronjob
spec:
  schedule: "*/1 * * * *"
  successfulJobsHistoryLimit: 5
  failedJobsHistoryLimit: 3
  startingDeadlineSeconds: 30
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: sleep
            image: busybox
            command: ["sleep"]
            args: ["10"]
          restartPolicy: Never
EOF
```

# CronJobハンズオン リソース確認
```
kubectl apply -f cronjob.yaml
```
```
kubectl get cronjobs
```
```
kubectl get job -w
```

# CronJobハンズオン リソース削除
```
kubectl delete -f cronjob.yaml
```