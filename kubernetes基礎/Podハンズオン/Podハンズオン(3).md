# Podハンズオン(3)環境変数の設定①
```
kubectl run nginx --image=nginx --env=demo_env=sample
```
```
kubectl describe pods nginx | grep -i env
```

# Podハンズオン(3)環境変数の設定②
```
cat > test2.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: env-nginx
  name: env-nginx
spec:
  containers:
  - image: nginx
    name: env-nginx
    env:
    - name: demo_env
      value: "sample"
EOF
```
```
kubectl create -f test2.yaml
```
```
kubectl delete -f test2.yaml
```
```
kubectl delete pods nginx
```
