# nfsクライアントインストール　※各node
```
apt-get install nfs-common
```
# Helmインストール
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
# nfs-subdir-external-provisioner のインストール
```
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
```
```
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --set nfs.server=<nfsのIP> \
    --set nfs.path=<マウントパス> \
    --set storageClass.name=<storageClassの名前を任意>
```
```
kubectl get storageclass
```
```
kubectl get pod
```
# nfs-subdir-external-provisioner のアンインストール
```
helm uninstall nfs-subdir-external-provisioner
```
