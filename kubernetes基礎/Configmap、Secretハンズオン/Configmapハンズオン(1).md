# Configmapハンズオン(1)作成方法②
```
kubectl create configmap test-configmap --from-literal=name=test --from-literal=hostname=testhost
```
```
kubectl describe configmap test-configmap
```

# Configmapハンズオン(1)作成方法③
```
cat > configmap.yaml << EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: yaml-configmap
data:
  name: "test"
  hostname: "testhost"
  test.html: |
    Hello world
EOF
```
```
kubectl apply -f configmap.yaml
```

# Configmapハンズオン(1)作成方法④
```
kubectl describe configmaps yaml-configmap
```
```
kubectl delete configmaps test-configmap
kubectl delete -f configmap.yaml
```
