# Taint Tolerationハンズオン②リソース作成
```
kubectl get node
```
```
kubectl taint node <node名1台目> taint=test:NoSchedule
```
```
kubectl describe node <node名1台目>
```
```
kubectl taint node <node名2台目> taint=test:NoSchedule
```
```
kubectl run nginx --image=nginx
```
```
kubectl get pods
```
```
kubectl delete pods nginx
```
```
cat > taint.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-taint
spec:
  containers:
  - name: nginx
    image: nginx
  tolerations:
  - key: taint
    value: test
    effect: NoSchedule
EOF
```
```
kubectl apply -f taint.yaml
```
```
kubectl get pods -o wide
```

# Taint Tolerationハンズオン③リソース削除
```
kubectl delete -f taint.yaml
```
```
kubectl taint node <node名1台目> taint-
```
```
kubectl taint node <node名2台目> taint-
```
