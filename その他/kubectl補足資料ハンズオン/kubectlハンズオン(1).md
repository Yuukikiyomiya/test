# kubectlハンズオン①リソース作成
```
cat > path-pod.yaml << EOF
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
---
apiVersion: v1
kind: Pod
metadata:
  name: redis
spec:
  containers:
  - name: redis
    image: redis
---
EOF
```
```
kubectl apply -f path-pod.yaml
```

# kubectlハンズオン②出力形式の指定
```
kubectl get pods -o yaml
```
```
kubectl get pods -o json
```

# kubectlハンズオン③JSON PATHの指定
```
kubectl get pods -o=jsonpath='{.items[0].metadata.name}{"\n"}'
```
```
kubectl get pods -o=jsonpath='{.items[1].metadata.name}{"\n"}'
```
```
kubectl get pods -o=jsonpath='{.items[*].metadata.name}{"\n"}'
```
```
kubectl get pods -o jsonpath='{.items[0].metadata.name}:{.items[0].metadata.namespace}{"\n"}'
```
```
kubectl get pods -o jsonpath='{range .items[*]}{@.metadata.name} : {@.metadata.namespace}{"\n"}{end}'
```

# kubectlハンズオン④kubectl top
```
kubectl apply -f https://gitlab.com/Yuukikiyomiya/metrics-server/-/raw/main/components.yaml
```

# kubectlハンズオン⑤リソース削除
```
kubectl delete -f path-pod.yaml
kubectl delete -f components.yaml
```
