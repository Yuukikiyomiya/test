```
apt install nfs-kernel-server
```
```
mkdir /nfs
```
```
cat << EOF > /etc/exports
/nfs <masterIP>(rw,no_root_squash) <node1IP>(rw,no_root_squash) <node2IP>(rw,no_root_squash)
EOF
```
```
systemctl start nfs-server.service
systemctl enable nfs-server.service
```
